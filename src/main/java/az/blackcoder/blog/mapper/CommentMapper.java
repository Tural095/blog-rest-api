package az.blackcoder.blog.mapper;

import az.blackcoder.blog.dto.CommentDto;
import az.blackcoder.blog.entity.Comment;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CommentMapper {

    CommentMapper INSTANCE= Mappers.getMapper(CommentMapper.class);

    CommentDto mapToDto(Comment comment);

    Comment mapToEntity(CommentDto commentDto);
}
