package az.blackcoder.blog.mapper;

import az.blackcoder.blog.dto.PostDto;
import az.blackcoder.blog.entity.Post;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface PostMapper {

    PostMapper INSTANCE= Mappers.getMapper(PostMapper.class);

    PostDto mapToDto(Post post);

    Post mapToEntity(PostDto postDto);
}
