package az.blackcoder.blog.controller;

import az.blackcoder.blog.dto.CommentDto;
import az.blackcoder.blog.service.CommentService;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Validated
@RequiredArgsConstructor
@RequestMapping("/api/comments")
@RestController
public class CommentController {

    private final CommentService commentService;

    @PostMapping("/{postId}")
    public CommentDto createComment(@PathVariable long postId, @Valid @RequestBody CommentDto commentDto) {
        return commentService.createComment(postId, commentDto);
    }

    @GetMapping("/all/{postId}")
    public List<CommentDto> getAllComment(@PathVariable long postId) {
        return commentService.getAllCommentByPostId(postId);
    }

    @GetMapping("/{postId}/{commentId}")
    public CommentDto getCommentById(@PathVariable long postId, @PathVariable long commentId) {
        return commentService.findCommentByPosId(postId, commentId);
    }

    @PutMapping("/{postId}/{commentId}")
    public CommentDto updateComment(@PathVariable long postId, @PathVariable long commentId,
                                    @Valid @RequestBody CommentDto commentDto) {
        return commentService.updateComment(postId, commentId, commentDto);
    }

    @DeleteMapping("/{postId}/{commentId}")
    public void deleteCommentById(@PathVariable long postId, @PathVariable long commentId) {
        commentService.deleteCommentById(postId, commentId);
    }
}
