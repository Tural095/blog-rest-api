package az.blackcoder.blog.service;

import az.blackcoder.blog.dto.PostDto;
import az.blackcoder.blog.entity.Post;

import java.util.List;

public interface PostService {

    PostDto createPost(PostDto postDto);

    List<PostDto> getAllPost();

    PostDto findPostById(Long id);

    Post findById(Long postId);

    PostDto updatePost(PostDto postDto, Long id);

    void deletePostById(Long id);

    List<PostDto> findByCategoryId(Long categoryId);
}
