package az.blackcoder.blog.service;

import az.blackcoder.blog.dto.CommentDto;

import java.util.List;

public interface CommentService {

    CommentDto createComment(long postId, CommentDto commentDto);

    List<CommentDto> getAllCommentByPostId(long postId);

    CommentDto findCommentByPosId(long postId, long commentId);

    CommentDto updateComment(long postId, long commentId,CommentDto commentDto);

    void deleteCommentById(long postId, long commentId);
}
