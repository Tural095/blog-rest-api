package az.blackcoder.blog.service.impl;

import az.blackcoder.blog.dto.PostDto;
import az.blackcoder.blog.entity.Category;
import az.blackcoder.blog.entity.Post;
import az.blackcoder.blog.exception.ResourceNotFoundException;
import az.blackcoder.blog.mapper.PostMapper;
import az.blackcoder.blog.repository.PostRepository;
import az.blackcoder.blog.service.CategoryService;
import az.blackcoder.blog.service.PostService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class PostServiceImpl implements PostService {

    private final PostRepository postRepository;
    private final CategoryService categoryService;

    @Override
    public PostDto createPost(PostDto postDto) {
        Category category = categoryService.findById(postDto.getCategoryId());
        Post post = PostMapper.INSTANCE.mapToEntity(postDto);
        post.setCategory(category);
        postRepository.save(post);
        return PostMapper.INSTANCE.mapToDto(post);
    }

    @Override
    public List<PostDto> getAllPost() {
        return postRepository.findAll().stream().map(PostMapper.INSTANCE::mapToDto).collect(Collectors.toList());
    }

    @Override
    public PostDto findPostById(Long id) {
        Post post = postRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException());
        return PostMapper.INSTANCE.mapToDto(post);
    }

    @Override
    public Post findById(Long postId) {
        Post post = postRepository.findById(postId).orElseThrow(() -> new ResourceNotFoundException());
        return post;
    }

    @Override
    public PostDto updatePost(PostDto postDto, Long id) {
        Category category = categoryService.findById(postDto.getCategoryId());
        Post post = postRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException());
        post.setCategory(category);
        post.setTittle(postDto.getTittle());
        post.setDescription(postDto.getDescription());
        post.setContent(postDto.getContent());

        postRepository.save(post);
        return PostMapper.INSTANCE.mapToDto(post);
    }

    @Override
    public void deletePostById(Long id) {
        postRepository.deleteById(id);
    }

    @Override
    public List<PostDto> findByCategoryId(Long categoryId) {
        categoryService.findById(categoryId);
        List<Post> byCategoryId = postRepository.findByCategoryId(categoryId);
        return byCategoryId.stream().map(PostMapper.INSTANCE::mapToDto).collect(Collectors.toList());
    }
}
