package az.blackcoder.blog.service.impl;

import az.blackcoder.blog.dto.CommentDto;
import az.blackcoder.blog.entity.Comment;
import az.blackcoder.blog.entity.Post;
import az.blackcoder.blog.exception.BlogApiException;
import az.blackcoder.blog.exception.ResourceNotFoundException;
import az.blackcoder.blog.mapper.CommentMapper;
import az.blackcoder.blog.repository.CommentRepository;
import az.blackcoder.blog.service.CommentService;
import az.blackcoder.blog.service.PostService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class CommentServiceImpl implements CommentService {

    private final CommentRepository commentRepository;
    private final PostService postService;

    @Override
    public CommentDto createComment(long postId, CommentDto commentDto) {
        Comment comment = CommentMapper.INSTANCE.mapToEntity(commentDto);
        Post post = getPost(postId);
        comment.setPost(post);
        commentRepository.save(comment);

        return CommentMapper.INSTANCE.mapToDto(comment);
    }

    @Override
    public List<CommentDto> getAllCommentByPostId(long postId) {
        return commentRepository.findByPostId(postId)
                .stream().map(CommentMapper.INSTANCE::mapToDto).collect(Collectors.toList());
    }

    @Override
    public CommentDto findCommentByPosId(long postId, long commentId) {
        Post post = getPost(postId);
        Comment comment = getComment(commentId);

        if (!comment.getPost().getId().equals(post.getId())) {
            throw new BlogApiException(HttpStatus.BAD_REQUEST, "Comment does not belong to post");
        }

        return CommentMapper.INSTANCE.mapToDto(comment);
    }



    @Override
    public CommentDto updateComment(long postId, long commentId, CommentDto commentDto) {
        Post post = getPost(postId);
        Comment comment = getComment(commentId);

        if (!comment.getPost().getId().equals(post.getId())) {
            throw new BlogApiException(HttpStatus.BAD_REQUEST, "Comment does not belong to post");
        }

        comment.setName(commentDto.getName());
        comment.setEmail(commentDto.getEmail());
        comment.setBody(commentDto.getBody());

        commentRepository.save(comment);

        return CommentMapper.INSTANCE.mapToDto(comment);

    }

    private Comment getComment(long commentId) {
        return commentRepository.findById(commentId).orElseThrow(() -> new ResourceNotFoundException());
    }

    private Post getPost(long postId) {
        return postService.findById(postId);
    }

    @Override
    public void deleteCommentById(long postId, long commentId) {
        Post post = getPost(postId);
        Comment comment = getComment(commentId);

        if (!comment.getPost().getId().equals(post.getId())) {
            throw new BlogApiException(HttpStatus.BAD_REQUEST, "Comment does not belong to post");
        }

        commentRepository.delete(comment);
    }
}
