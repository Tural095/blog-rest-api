package az.blackcoder.blog.service.impl;

import az.blackcoder.blog.dto.CategoryDto;
import az.blackcoder.blog.entity.Category;
import az.blackcoder.blog.exception.ResourceNotFoundException;
import az.blackcoder.blog.mapper.CategoryMapper;
import az.blackcoder.blog.repository.CategoryRepository;
import az.blackcoder.blog.service.CategoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class CategoryServiceImpl implements CategoryService {

    private final CategoryRepository categoryRepository;

    @Override
    public CategoryDto addCategory(CategoryDto categoryDto) {
        Category category = CategoryMapper.INSTANCE.toEntity(categoryDto);
        categoryRepository.save(category);
        return CategoryMapper.INSTANCE.toDto(category);
    }

    @Override
    public CategoryDto findCategoryById(Long id) {
        Category category = findById(id);
        return CategoryMapper.INSTANCE.toDto(category);
    }

    @Override
    public List<CategoryDto> findAllCategories() {
        return categoryRepository.findAll()
                .stream().map(CategoryMapper.INSTANCE::toDto).collect(Collectors.toList());
    }

    @Override
    public CategoryDto updateCategoryById(Long id, CategoryDto categoryDto) {
        Category category = findById(id);
        category.setName(categoryDto.getName());
        category.setDescription(categoryDto.getDescription());
        category.setId(categoryDto.getId());
        categoryRepository.save(category);
        return CategoryMapper.INSTANCE.toDto(category);
    }

    @Override
    public void deleteCategoryById(Long id) {
        categoryRepository.deleteById(id);
    }

    @Override
    public Category findById(Long categoryId) {
        return categoryRepository.findById(categoryId).orElseThrow(() -> new ResourceNotFoundException());
    }

}
