package az.blackcoder.blog.service;

import az.blackcoder.blog.dto.CategoryDto;
import az.blackcoder.blog.entity.Category;

import java.util.List;

public interface CategoryService {

    CategoryDto addCategory(CategoryDto categoryDto);

    CategoryDto findCategoryById(Long id);

    List<CategoryDto> findAllCategories();

    CategoryDto updateCategoryById(Long id, CategoryDto categoryDto);

    void deleteCategoryById(Long id);

    Category findById(Long categoryId);
}
