package az.blackcoder.blog.dto;

import lombok.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SignUpDto {

    private String name;
    private String username;
    private String email;
    private String password;
}
