package az.blackcoder.blog.dto;

import lombok.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class JWTAuthResponse {

    private String accessToken;
    private String tokenType;

    public JWTAuthResponse(String accessToken) {
        this.accessToken = accessToken;
    }
}
